package org.ovirt.optimizer.service;

import org.ovirt.optimizer.common.Result;

public interface OptimizerServiceLocal {
    Result getCurrentResult(String cluster);
}

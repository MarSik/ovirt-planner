package org.ovirt.optimizer.service;

import org.jboss.logging.Logger;
import org.ovirt.engine.sdk.Api;
import org.ovirt.engine.sdk.exceptions.ServerException;
import org.ovirt.engine.sdk.exceptions.UnsecuredConnectionAttemptError;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Add the following to your Jboss configuration:
 *
 <system-properties>
 <property name="org.ovirt.optimizer.sdk.server" value="euryale.brq.redhat.com"/>
 <property name="org.ovirt.optimizer.sdk.port" value="8080"/>
 <property name="org.ovirt.optimizer.sdk.username" value="admin"/>
 <property name="org.ovirt.optimizer.sdk.password" value="letmein"/>
 <property name="org.ovirt.optimizer.sdk.ca.store" value="/home/msivak/Jboss/standalone/configuration/ovirt.truststore"/>
 </system-properties>
 */
public class OvirtClient {
    @Inject
    private Logger log;

    String server;
    String port;
    String username;
    String password;
    String caStore;

    public OvirtClient() {
        this.server = System.getProperty("org.ovirt.optimizer.sdk.server");
        this.port = System.getProperty("org.ovirt.optimizer.sdk.port");
        this.username = System.getProperty("org.ovirt.optimizer.sdk.username");
        this.password = System.getProperty("org.ovirt.optimizer.sdk.password");
        this.caStore = System.getProperty("org.ovirt.optimizer.sdk.ca.store");
    }

    public Api connect()
            throws UnsecuredConnectionAttemptError, ServerException, IOException {
        String url = String.format("http://%s:%s/ovirt-engine/api", server, port);
        log.debug(String.format("Logging to %s as %s", url, username));
        return new Api(url, username, password, false);
    }
}

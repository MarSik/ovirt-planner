package org.ovirt.optimizer.service;

import java.util.Set;

public interface OptimizerServiceManagement {
    Set<String> getAllClusters();
    Set<String> getActiveClusters();
}

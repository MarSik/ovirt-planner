package org.ovirt.optimizer.service;

import org.jboss.annotation.ejb.Management;
import org.jboss.annotation.ejb.Service;
import org.jboss.logging.Logger;
import org.ovirt.engine.sdk.entities.Host;
import org.ovirt.optimizer.common.Result;
import org.ovirt.optimizer.service.problemspace.OptimalDistributionSolution;
import org.ovirt.optimizer.service.problemspace.VmAssignment;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static javax.ejb.ConcurrencyManagementType.BEAN;

/**
 * This is the main service class for computing the optimized
 * Vm to host assignments.
 */
@Startup
@Singleton
@Local(OptimizerServiceLocal.class)
@Remote(OptimizerServiceRemote.class)
@Service
@Management(OptimizerServiceManagement.class)
@ConcurrencyManagement(BEAN)
public class OptimizerServiceBean implements OptimizerServiceManagement, OptimizerServiceLocal, OptimizerServiceRemote {
    @Inject
    private Logger log;

    ClusterDiscovery discovery;
    Timer discoveryTimer;
    Set<Thread> threads;
    OvirtClient client;

    // This attribute is used by the exported API and has to
    // be used in thread safe way
    final Map<String, ClusterOptimizer> clusterOptimizers = new HashMap<>();

    @Resource
    private javax.ejb.SessionContext context;

    @PostConstruct
    public void create() throws Exception {
        log.info("oVirt optimizer service starting");
        threads = new HashSet<>();
        client = new OvirtClient();
        discovery = new ClusterDiscovery(client);
        TimerConfig timerConfig = new TimerConfig();
        timerConfig.setInfo("clusterDiscoveryTimer");
        timerConfig.setPersistent(false);
        discoveryTimer = context.getTimerService().createIntervalTimer(0, 120000, timerConfig);
    }

    // Synchronized should not be needed, but is here as a
    // safeguard for prevention from threading mistakes
    @Timeout
    public synchronized void discoveryTimeout(final Timer timer){
        // Check for possible spurious timeouts from old instances
        if (timer != discoveryTimer) {
            log.warn(String.format("Unknown timeout from %s", timer.toString()));
            return;
        }

        log.info("Discovering clusters...");
        Set<String> availableClusters = discovery.getClusters();
        if (availableClusters == null) {
            log.error("Cluster discovery failed");
            return;
        }

        Set<String> missingClusters;

        synchronized (clusterOptimizers) {
            /* Compute a set of clusters that disappeared */
            missingClusters = new HashSet<>(clusterOptimizers.keySet());
            missingClusters.removeAll(availableClusters);

            /* Compute a set of new clusters */
            availableClusters.removeAll(clusterOptimizers.keySet());
        }

        for (String clusterId: availableClusters) {
            log.info(String.format("New cluster %s detected", clusterId));

            ClusterOptimizer planner = new ClusterOptimizer(client, clusterId, new ClusterOptimizer.Finished() {
                @Override
                public void solvingFinished(ClusterOptimizer planner, Thread thread) {
                    threads.remove(thread);
                }
            });

            Thread updater = new Thread(planner.getUpdaterInstance());
            Thread solver = new Thread(planner);

            updater.start();
            threads.add(updater);

            solver.start();
            threads.add(solver);

            synchronized (clusterOptimizers) {
                clusterOptimizers.put(clusterId, planner);
            }
        }

        synchronized (clusterOptimizers) {
            for (String clusterId: missingClusters) {
                clusterOptimizers.get(clusterId).terminate();
                clusterOptimizers.get(clusterId).getUpdaterInstance().terminate();
                log.info(String.format("Cluster %s was removed", clusterId));
            }
        }
    }

    @PreDestroy
    public void stop() {
        log.info("oVirt service service stopping");
        discoveryTimer.cancel();

        synchronized (clusterOptimizers) {
            for (ClusterOptimizer clusterOptimizer: clusterOptimizers.values()) {
                clusterOptimizer.getUpdaterInstance().terminate();
                clusterOptimizer.terminate();
            }
        }

        log.info("Waiting for threads to finish");
        // Iterate over copy of the set as the ending threads will
        // be removed by callback
        for (Thread thread: new ArrayList<>(threads)) {
            try {
                thread.join();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        log.info("oVirt service service stopped");
    }

    @Override
    public Set<String> getAllClusters() {
        synchronized (clusterOptimizers) {
            return clusterOptimizers.keySet();
        }
    }

    @Override
    public Set<String> getActiveClusters() {
        synchronized (clusterOptimizers) {
            return clusterOptimizers.keySet();
        }
    }

    @Override
    public Result getCurrentResult(String cluster) {
        ClusterOptimizer clusterOptimizer;
        Result r = new Result();
        r.setCluster(cluster);

        synchronized (clusterOptimizers) {
            clusterOptimizer = clusterOptimizers.get(cluster);
        }

        if (clusterOptimizer == null) {
            log.error(String.format("Cluster %s does not exist", cluster));
            r.setHostToVms(new HashMap<String, ArrayList<String>>());
            r.setVmToHost(new HashMap<String, String>());
        }
        else {
            Map<String, ArrayList<String>> hostToVms = new HashMap<>();
            Map<String, String> vmToHost = new HashMap<>();

            OptimalDistributionSolution best = clusterOptimizer.getBestSolution();
            r.setHosts(new HashSet<String>());
            for (Host host: best.getHosts()) {
                hostToVms.put(host.getId(), new ArrayList<String>());
                r.getHosts().add(host.getId());
            }
            for (VmAssignment vm: best.getVms()) {
                hostToVms.get(vm.getHost().getId()).add(vm.getVm().getId());
                vmToHost.put(vm.getId(), vm.getHost().getId());
            }
            r.setHostToVms(hostToVms);
            r.setVmToHost(vmToHost);
        }

        return r;
    }
}

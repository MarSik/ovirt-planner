(
cat <<EOF
/system-property=org.ovirt.optimizer.sdk.server:add(value=localhost)
/system-property=org.ovirt.optimizer.sdk.port:add(value=8080)
/system-property=org.ovirt.optimizer.sdk.username:add(value=admin@internal)
/system-property=org.ovirt.optimizer.sdk.password:add(value=letmein)
/system-property=org.ovirt.optimizer.sdk.ca.store:add(value=/home/user/Jboss/standalone/configuration/ovirt.truststore)
EOF
) | ./bin/jboss-cli.sh --connect controller=localhost

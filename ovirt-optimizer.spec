%global engine_etc /etc/ovirt-engine
%global engine_data %{_datadir}/ovirt-engine
%global jboss_deployments %{_datadir}/jboss-as/standalone/deployments

Name:		ovirt-optimizer
Version:	0.1
Release:	1%{?dist}
Summary:	Cluster balance optimization service for oVirt
Group:		%{ovirt_product_group}
License:	ASL 2.0
URL:		http://www.ovirt.org
Source0:	http://ovirt.org/releases/stable/src/%{name}-%{version}.tar.gz

BuildArch:	noarch

BuildRequires:	java-devel
BuildRequires:	jpackage-utils
BuildRequires:	maven-local
BuildRequires:  maven-war-plugin
BuildRequires:  maven-jar-plugin
BuildRequires:  maven-ejb-plugin
BuildRequires:	unzip

Requires:	java-headless >= 1:1.7.0
Requires:	jboss-as >= 7.1.1-9.3
Requires:	jpackage-utils
Requires:       resteasy
Requires:       jackson

%description
%{name} service collects data from the oVirt engine and proposes
Vm to host assignments to utilize cluster resources better.

%package ui
Summary:        UI for displaying results in oVirt webadmin
Requires:	%{name}-webadmin-portal >= 3.4

%description ui
This subpackage adds an UI plugin to the oVirt webadmin portal.
This plugin then adds a Tab to the cluster display and presents
the assingment recommendations to the sysadmin there.

%prep
%setup -c -q

%build
mvn --offline clean install

%install
# Remove the bundled jars by moving them elsewhere and then
# copying only the needed files back. The rest is provided
# by this package's dependencies.
mkdir target/lib
mv target/%{name}/WEB-INF/lib/* target/lib

# Install the exploded war to javadir
install -dm 755 %{buildroot}%{_javadir}/%{name}
cp -ar target/%{name} %{buildroot}%{_javadir}

# Copy bundled libs to %{buildroot}%{_javadir}/%{name}/WEB-INF/lib

# Symlink it to Jboss war directory and touch the deploy marker
install -dm 755 %{buildroot}%{jboss_deployments}
ln -sf %{_javadir}/%{name} %{buildroot}%{jboss_deployments}/%{name}.war
touch %{buildroot}%{jboss_deployments}/%{name}.war.dodeploy

# Copy the setup script to documentation
install -dm 755 %{buildroot}%{_docdir}/%{name}-%{version}
install dist/jboss-setup.sh %{buildroot}%{_docdir}/%{name}-%{version}

# Install the UI plugin to the oVirt webadmin
install -dm 755 %{buildroot}%{engine_data}/ui-plugins
install -dm 755 %{buildroot}%{engine_etc}/ui-plugins
install -dm 755 %{buildroot}%{engine_data}/ui-plugins/ovirt-optimizer-resources
install dist/ovirt-optimizer-uiplugin/*.json %{buildroot}%{engine_data}/ui-plugins/
install dist/ovirt-optimizer-uiplugin/ovirt-optimizer-resources/* %{buildroot}%{engine_data}/ui-plugins/ovirt-optimizer-resources
install dist/etc/*.json %{buildroot}%{engine_etc}/ui-plugins/


%files
%doc README COPYING
%dir %{_javadir}/%{name}
%{_javadir}/%{name}/*
%{jboss_deployments}/*

%files ui
%dir %{engine_data}/ui-plugins/ovirt-optimizer-resources
%config %{engine_etc}/ui-plugins/ovirt-optimizer-config.json
%{engine_data}/ui-plugins/ovirt-optimizer.json
%{engine_data}/ui-plugins/ovirt-optimizer-resources/*
